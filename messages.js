Messages = {
	antiFloodInfo: false,
	antiFloodBlock: false,
	antiFloodTimeout: {
		user: 7,
		other: 2
	},

	messagesBlocked: false,
	messagesForDonators: false,

	lastMsgId: 0,

	publicChatFontSize: 11,
	privateChatFontSize: 13,

	maxMessagesCount: 50,
	messagesForAnonymous: 0,
	anonymousMessagesCounter: 0,

	publicMsgInput: '#messageInput .msgInput',
	publicSendBtn: '#messageInput .sendButton',
	privateMsgInput: '#privMessageInput .msgInput',
	privateSendBtn: '#privMessageInput .sendButton',

	/**
	 * Inicjalizacja
	 */
	init: function(mode){
		if(this.messagesForAnonymous > 0){
			if(anonMsgs = $.cookie('__lc_amsg')) this.anonymousMessagesCounter = $.base64.decode(anonMsgs);
		}
		if(mode == 'public'){
			$(Messages.publicMsgInput).keypress(function(e){
				var code = (e.keyCode ? e.keyCode : e.which);
				if(code == 10 || code == 13) {
					Messages.publicSend();
					return false;
				}
			});

			$(Messages.publicSendBtn).click(function(){
				 Messages.publicSend();
			});
			if($.cookie('publicChatFontSize')){
				Messages.publicChatFontSize = parseInt($.cookie('publicChatFontSize'));
			} else{
				var fontSize = $(".messagesList").css("font-size");
				$.cookie('publicChatFontSize', fontSize, { expires: 365, path: '/'} )
			}
			Messages.setFontSize(mode, Messages.publicChatFontSize);
		}
		else if(mode == 'private'){
			$(Messages.privateMsgInput).keypress(function(e){
				var code = (e.keyCode ? e.keyCode : e.which);
				if(code == 10 || code == 13) {
					Messages.privateSend();
					return false;
				}
			});

			$(Messages.privateSendBtn).click(function(){
				 Messages.privateSend();
			});
			if($.cookie('privateChatFontSize')){
				Messages.privateChatFontSize = parseInt($.cookie('privateChatFontSize'));
			} else{
				var fontSize = $("#privMessagesList").css("font-size");
				$.cookie('privateChatFontSize', fontSize, { expires: 365, path: '/'} )
			}
			Messages.setFontSize(mode, Messages.privateChatFontSize);
		}
	},

	/**
	 * Odpisanie użytkownikowi
	 */
	replyTo: function(mode, nick){
		if(mode == 'public'){
			$(Messages.publicMsgInput).focus().val(nick+': ');
		}
		else if(mode == 'private'){
			$(Messages.privateMsgInput).focus().val(nick+': ');
		}
	},

	/**
	 * Wysłanie wiadomości publicznej
	 */
	publicSend: function(){
		var message = $(Messages.publicMsgInput).val();
		if(message.length > 0){
			var timeout = Messages.antiFloodTimeout.user;
			if(User.role != 'user')
				timeout = Messages.antiFloodTimeout.other;

			if(User.role == 'anonym'){
				if(this.anonymousMessagesCounter >= this.messagesForAnonymous){
					$('.chatForLogged').show();
					return false;
				}
				else{
					$.cookie('__lc_amsg', $.base64.encode(++this.anonymousMessagesCounter), {expires: 30});
				}
			}

			if(this.antiFloodBlock){
				$('.antiFloodAmount').text(timeout);
				Animation.show('.antiFlood', 'antiFlood');
			}
			else if(this.messagesBlocked && (User.role != 'admin' || User.role != 'superuser' )){}
			else if(this.messagesForDonators && (User.role == 'user' || User.role == 'anonym')){}
			else {
				Broadcast.loadData(message, 'public');
				$(Messages.publicMsgInput).val('');
				this.antiFloodBlock = true;
				$('.antiFloodAmount').text(timeout);
				Timer.countDown($(Messages.publicMsgInput), timeout, {
					onlySecs: true,
					afterCounting: Messages.floodUnlock,
					resetValue: true
				});
			}
		}
		return true;
	},

	/**
	 * Wysłanie wiadomości prywatnej
	 */
	privateSend: function(){
		var message = $(Messages.privateMsgInput).val();
		if(message.length > 0){
			var timeout = Messages.antiFloodTimeout.other;

			if(this.antiFloodBlock){}
			else {
				Broadcast.loadData(message, 'private');
				$(Messages.privateMsgInput).val('');
				this.antiFloodBlock = true;
				$('.antiFloodAmount').text(timeout);
				Timer.countDown($('.antiFloodAmount'), timeout, {
					onlySecs: true,
					afterCounting: Messages.floodUnlock,
					resetValue: true
				});
			}
		}
	},

	/**
	 * Odblokowanie wysyłania wiadomości
	 */
	floodUnlock: function(){
		Messages.antiFloodBlock = false;
		Animation.hide('.antiFlood', 'antiFlood');
	},

	/**
	 * Aktualizajca publicznego chata
	 * @param msg
	 */
	updatePublicChat: function(msg){
		Messages.lastMsgId = msg.lastMsgId;
		// informacje o stanie chata
		if(User.logged || (!User.logged && this.messagesForAnonymous > 0)){
			Messages.messagesBlocked = msg.blocked;
			Messages.messagesForDonators = msg.forDonators;

			if(msg.blocked)
				Animation.show('.chatBlocked', 'chatBlocked');
			else
				Animation.hide('.chatBlocked', 'chatBlocked');
			if(!msg.blocked && msg.forDonators)
				Animation.show('.chatForDonators', 'chatForDonators');
			else
				Animation.hide('.chatForDonators', 'chatForDonators');
		}

		if(msg.list){
			for(var i in msg.list){
				if($('#message_'+i).length==0){
					this.addMessage('public', msg.list[i]);
					this.updateFlashChat(msg.pureList[i]);
				}
			}
			maxCount = this.maxMessagesCount;
			if($('#messagesList .message').size() > maxCount) {
				$('#messagesList .message:gt('+maxCount+')').detach();
			}

		}
		if(msg.removeId){
			for(var i in msg.removeId){
				$('#message_'+msg.removeId[i]).detach();
			}
		}
	},

	/**
	 * Aktualizajca prywatnego chata
	 * @param msg
	 */
	updatePrivateChat: function(msg){

		if(msg){
			for(i in msg){
				if($('#message_'+i).length==0){
					this.addMessage('private', msg[i]);
				}
			}
			maxCount = this.maxMessagesCount;
			if($('#privMessagesList .message').size() > maxCount) {
				$('#privMessagesList .message:gt('+maxCount+')').detach();
			}

		}
	},

	/**
	 * Dodaje wiadomość czata na paletę wiadomości
	 * @param {String} mode
	 * @param {String} message
	 */
	addMessage: function(mode, message){console.log(1);
		$message = $(message);
		if(origSrc = $message.find('img').attr('data-src')){
			$message.find('img').attr('src', origSrc);
		}
		switch(mode){
			case 'public':
				$('#messagesList').prepend($message);
				break;
			case 'private':
				$('#privMessagesList').prepend($message);
				break;
		}
	},

	/**
	 * Usuwa wiadomości użytkownika z czata
	 */
	removeMessages: function(userId){
		$('#messagesList .user_'+userId).detach();
	},

	/**
	 *
	 * @param {Object} obj
	 * @returns {String|@exp;@call;String}
	 */
	msgToString: function(obj){
		var t = typeof (obj);
		if (t != "object" || obj === null) {
			// simple data type
			if (t == "string") obj = '"'+obj+'"';
			return String(obj);
		}
		else {
			// recurse array or object
			var n, v, json = [], arr = (obj && obj.constructor == Array);
			for (n in obj) {
				v = obj[n]; t = typeof(v);
				if (t == "string") v = '"'+v+'"';
				else if (t == "object" && v !== null) v = JSON.stringify(v);
				json.push((arr ? "" : '"' + n + '":') + String(v));
			}
			return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
		}
	},


	/*
	 * Zwiększa czcionkę w wiadomościach
	 * @param {Number) count
	 */
	increaseFontSize: function(mode, count){
		if(count != 0){
			if(mode == "public" && Messages.publicChatFontSize <= 26){
				Messages.publicChatFontSize += count;
				Messages.setFontSize(mode, Messages.publicChatFontSize);
			}
			if(mode == "private" && Messages.privateChatFontSize <= 26){
				Messages.privateChatFontSize += count;
				Messages.setFontSize(mode, Messages.privateChatFontSize);
			}
		}
	},

	/*
	 * Zmniejsza czcionkę w wiadomościach
	 * @param {Number) count
	 */
	decreaseFontSize: function(mode, count){
		if(count != 0){
			if(mode == "public" && Messages.publicChatFontSize >= 11){
				Messages.publicChatFontSize -= count;
				Messages.setFontSize(mode, Messages.publicChatFontSize);
			}
			if(mode == "private" && Messages.privateChatFontSize >= 11){
				Messages.privateChatFontSize -= count;
				Messages.setFontSize(mode, Messages.privateChatFontSize);
			}
		}
	},

	/*
	 * Ustawia czcionkę w wiadomościach
	 * @param {Number) size
	 */
	setFontSize: function(mode, size){
		if(mode == "public"){
			$.cookie("publicChatFontSize", size, { expires: 365, path: '/'  });
			$('.messagesList').css('font-size', size);
		}
		if(mode == "private"){
			$.cookie("privateChatFontSize", size, { expires: 365, path: '/' });
			$('#privMessagesList').css('font-size', size);
		}
	},

	updateFlashChat: function(message){
		try {
			if(flashMovie = document.getElementById('PublicCam')){
				if(typeof flashMovie.chatOnScreen == 'function'){
					flashMovie.chatOnScreen(this.msgToString(message));
				}
			}
		} catch(err){

		}
	}
};
