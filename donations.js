Donations = {
	dialogInstance: null,
	bankAmount: 0,
	minDonation: 10,
	maxDonation: null,
	defaultDonation: 10,
	allowDonation: true,
	isPrivate: false,

	profileId: null,
	broadcastId: null,

	/**
	 * Inicjalizacja
	 */
	init: function(){
		$('.float_donationAmount').val(this.defaultDonation);
	},

	/**
	 * Pokazuje okno
	 */
	showWindow: function(isPrivate){
		this.broadcastId = Broadcast.broadcastId;
		this.isPrivate = isPrivate ? 1 : 0;
		var content;
		if(User.primaryBankAmount < this.minDonation){
			this.allowDonation = false;

			content = $('#modal_donation .tooPoor').html();
			this.dialogInstance = Dialog.show(Default.donations.giveDonationTitle, content, null, {
				resizable: false,
				buttons: [
					{
						text: 'Zamknij',
						'class': Default.buttons.cancelClass,
						click: function(){ Donations.cancelWindow(); }
					}
				]
			});


		}
		else {
			this.allowDonation = true;

			content = $('#modal_donation .hasMoney').html();
			this.dialogInstance = Dialog.show(Default.donations.giveDonationTitle, content, null, {
				resizable: false,
				buttons: [
					{
						text: Default.donations.giveDonationButton,
						'class': Default.buttons.acceptClass,
						click: function(){ Donations.send(); }
					},
					{
						text: 'Anuluj',
						'class': Default.buttons.cancelClass,
						click: function(){ Donations.cancelWindow(); }
					}
				]
			});

			this.dialogInstance.find(".minDonation").text(this.defaultDonation);
			this.dialogInstance.find(".maxDonation")
					.text(this.maxDonation && User.primaryBankAmount > this.maxDonation
							? this.maxDonation
							: User.primaryBankAmount);
			this.dialogInstance.find("#donationAmount")
				.val(this.defaultDonation)
				.on('keypress', function(e){
					var code = (e.keyCode ? e.keyCode : e.which);
					if(code == 10 || code == 13) {
						Donations.send();
					}
				});
		}
//		this.showModal(curtain);
	},

	/**
	 * Napiwek dla profilu
	 */
	donateProfile: function(profileId){
		this.profileId = profileId;
		User.updateBankAmount();
		Donations.showWindow();
	},

	/**
	 * Anulowanie okna
	 */
	cancelWindow: function(){
		$(this.dialogInstance).dialog('close');
	},

	/**
	 * Przesyłanie gotówki
	 */
	send: function(){
		if(this.allowDonation){
			var donation = parseInt(this.dialogInstance.find("#donationAmount").val());
			var log = {
				donation: donation,
				userPrimaryAmount: User.primaryBankAmount,
				userSecondaryAmount: User.secondaryBankAmount
			};
//			console.log(donation);
			if(isNaN(donation)){
				this.dialogInstance.find("#donationAmount").val(Donations.defaultDonation);
				Dialog.info(Default.donations.mustBeANumber);
				Error.report(
					$.extend(true, log, {message: Default.donations.mustBeANumber}),
					'Donations.send'
				);
			}
			else if(donation < this.minDonation){
				this.dialogInstance.find("#donationAmount").val(Donations.defaultDonation);
				var text = Default.donations.tooLowDonation;
				text = text.replace('[minDonation]', Donations.minDonation);
				Dialog.info(text);
				Error.report(
					$.extend(true, log, {message: text}),
					'Donations.send'
				);
			}
			else if(this.maxDonation && donation > this.maxDonation){
				this.dialogInstance.find("#donationAmount").val(Donations.maxDonation);
				var text = Default.donations.maxDonationAmount;
				text = text.replace('[maxDonation]', Donations.maxDonation);
				Dialog.info(text);
				Error.report(
					$.extend(true, log, {message: text}),
					'Donations.send'
				);
			}
			else if(donation > User.primaryBankAmount){
				this.dialogInstance.find("#donationAmount").val(Donations.defaultDonation);
				Dialog.info(Default.donations.notEnoughMoney);
				Error.report(
					$.extend(true, log, {message: Default.donations.notEnoughMoney}),
					'Donations.send'
				);
			}
			else {
				this._donate(donation);
				$(this.dialogInstance).dialog('close');
			}
		}
	},

	/**
	 * Pokazuje dymek z napiwkiem
	 * @param {source} elem Element HTML przekazany jako parametr = this
	 * @param {Number} top
	 * @param {Number} left
	 */
	showCloud: function(elem, top, left){
		if(elem) elem = $(elem);

		if(!$("#float_donation").data('timeout')){
			$("#float_donation").data('timeout', null);
		} else {
			clearTimeout($("#float_donation").data('timeout'));
		}

		if(User.primaryBankAmount == 0){
			$("#float_donation .tooPoor").css('display','block');
			$("#float_donation .hasMoney").css('display','none');
		} else{
			$("#float_donation .hasMoney").css('display','block');
			$("#float_donation .tooPoor").css('display','none');

			$("#float_donation .minDonation").text(this.defaultDonation);
			$("#float_donation .maxDonation")
					.text(this.maxDonation && User.primaryBankAmount > this.maxDonation
							? this.maxDonation
							: User.primaryBankAmount);
		}
		if(elem){
			$("#float_donation").css({
				display: 'block',
				top: elem.position().top + top,
				left: elem.position().left + left
			});
		}

	},

	/**
	 * Chowa dymek z napiwkiem
	 * @param {Number} time Czas zamknięcia dymka
	 */
	hideCloud: function(time){
		time = time || 1000;
		$("#float_donation").data('timeout',
				setTimeout(function(){
						$("#float_donation").hide();
					}, time
				)
			);
	},

	/**
	 * Obsługa przycisku z podaną wartością napiwku
	 * @param {Number} val
	 */
	cloud_sendDonation: function(profileId, broadcastId, val, isPrivate, fromCloud, selector){
		this.profileId = profileId;
		this.broadcastId = broadcastId;
		this.isPrivate = isPrivate ? 1 : 0;
		if(selector)
			val = parseInt($(selector).val());
		if(val == "all")
			val = User.primaryBankAmount;

		var log = {
			donation: val,
			userPrimaryAmount: User.primaryBankAmount,
			userSecondaryAmount: User.secondaryBankAmount
		};

		if(isNaN(val)){
			$('.float_donationAmount').val(this.defaultDonation);
			Dialog.info(Default.donations.mustBeANumber);
			Error.report(
				$.extend(true, log, {message: Default.donations.mustBeANumber}),
				'Donations.cloud_sendDonation'
			);
		}
		else if(val < this.minDonation){
			$('.float_donationAmount').val(this.defaultDonation);
			var text = Default.donations.tooLowDonation;
			text = text.replace('[minDonation]', Donations.minDonation);
			Dialog.info(text);
			Error.report(
				$.extend(true, log, {message: text}),
				'Donations.cloud_sendDonation'
			);
		}
		else if(this.maxDonation && val > this.maxDonation){
			$('.float_donationAmount').val(this.defaultDonation);
			var text = Default.donations.maxDonationAmount;
			text = text.replace('[maxDonation]', Donations.maxDonation);
			Dialog.info(text);
			Error.report(
				$.extend(true, log, {message: text}),
				'Donations.cloud_sendDonation'
			);
		}
		else if(val > User.primaryBankAmount){
			$('.float_donationAmount').val(this.defaultDonation);
			Dialog.info(Default.donations.notEnoughMoney);
			Error.report(
				$.extend(true, log, {message: Default.donations.notEnoughMoney}),
				'Donations.cloud_sendDonation'
			);
		}
		else {
			this._donate(val);
			this.setFocus();
			$('.float_donationAmount').val(this.defaultDonation);
			if(fromCloud){
				this.hideCloud(10);
			}
		}
	},

	/**
	 * Obsługa naciśnięcia klawisza na polu wysokości napiwku
	 * @param {type} elem this
	 * @param {type} event event
	 * @param {Boolean} fromCloud true|false
	 */
	cloud_keyDonation: function(elem, event, profileId, broadcastId, isPrivate, fromCloud){
		if(event.keyCode == 10 || event.keyCode == 13) {
			Donations.cloud_sendDonation(profileId, broadcastId, parseInt($(elem).val()), isPrivate, fromCloud);
		}
	},

	/**
	 * Ustawia focus na pole czata
	 */
	setFocus: function(){
		if(this.isPrivate){
			$(Messages.privateMsgInput).focus();
		} else {
			$(Messages.publicMsgInput).focus();
		}
	},

	/**
	 * Wysyła napiwek odbiorcy
	 * @param {Number} donation
	 */
	_donate: function(donation){
		if(this.disallowDonation(donation)) return;
		$.ajax({
			url: '/ajax/donate',
			type: 'POST',
			dateType: 'json',
			data: {
				broadcastId: Donations.broadcastId,
				broadcasterId: Donations.profileId,
				privBroadcastHash: Private.hash,
				amount: donation,
				isPrivate: Donations.isPrivate
			},
			success: function(response){
//				console.log(response);
			},
			error: function(data){
				Error.report(data, 'Donations.send');
//				console.log('error');
			}
		});
	},

	/**
	 * Wstrzymuje napiwek
	 * @param {Number} val
	 * @returns {Boolean}
	 */
	disallowDonation: function(val){
		return false;
	}
};

$(function(){
	Donations.init();
});